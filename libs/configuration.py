from enum import Enum
from evdev import ecodes

from utils.singleton import Singleton


class ConfigurationParameter(Enum):
    CAMERA = "camera"
    MAX_CAMERA_FPS = "max_camera_fps"
    DEBUG = "debug"
    DISPLAY_FACES = "display_faces"
    FACE_SHAPE_PREDICTOR = "face_shape_predictor"
    JOYSTICK_DEVICE_NAME = "joystick"
    RESET_POSITION_KEY = "reset_position_key"


class Configuration(metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        self._data = {
            "camera": 0,
            "max_camera_fps": 30,
            "face_shape_predictor": "shape_predictor_68_face_landmarks.dat",
            "joystick": "Logitech Logitech Extreme 3D",
            "reset_position_key": ecodes.BTN_PINKIE
        }

    def set(self, parameter: ConfigurationParameter, value):
        self._data[parameter.value] = value

    def get(self, parameter: ConfigurationParameter, default=None):
        return self._data.get(parameter.value, default)

    def __getitem__(self, parameter: ConfigurationParameter):
        return self._data[parameter.value]
