from evdev import ecodes, InputDevice
from select import select
from threading import Thread


class ListenDevice(Thread):
    READ_TIMEOUT = 0.1

    def __init__(self, device_path):
        self._device_path = device_path
        self._device = None
        self._listen = True
        self._subscriptions = {}
        super().__init__()

    def start(self):
        self._device = InputDevice(self._device_path)
        super().start()

    def stop(self):
        self._listen = False

    def run(self):
        while self._listen:
            read, _writen, _ex = select([self._device.fd], [], [], self.READ_TIMEOUT)
            if read:
                for event in self._device.read():
                    if event.type == ecodes.EV_KEY and event.value == 1:
                        self._key_pressed(event.code)

    def subscribe(self, key_code, method):
        if key_code in self._subscriptions:
            self._subscriptions[key_code].append(method)
        else:
            self._subscriptions[key_code] = [method]

    def _key_pressed(self, key_code):
        if key_code in self._subscriptions:
            for method in self._subscriptions[key_code]:
                method()
