class BaseDeviceManagerException(Exception):
    pass


class NoDeviceException(BaseDeviceManagerException):
    pass
