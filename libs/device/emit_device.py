from evdev import UInput, AbsInfo, ecodes


class EmitDevice(UInput):
    NAME = "head-tracker"

    MIN_VALUE = 0
    MAX_VALUE = 1023
    CENTRAL_VALUE = MAX_VALUE // 2 + 1

    def __init__(self):
        # TODO: initial value should be configurable to clarify initial position
        self._events = {
            ecodes.EV_ABS: [
                (
                    ecodes.ABS_X,
                    AbsInfo(
                        value=self.CENTRAL_VALUE, min=self.MIN_VALUE, max=self.MAX_VALUE,
                        fuzz=0, flat=0, resolution=0
                    )
                ),
                (
                    ecodes.ABS_Y,
                    AbsInfo(
                        value=self.CENTRAL_VALUE, min=self.MIN_VALUE, max=self.MAX_VALUE,
                        fuzz=0, flat=0, resolution=0
                    )
                ),
                (
                    ecodes.ABS_Z,
                    AbsInfo(
                        value=self.MIN_VALUE, min=self.MIN_VALUE, max=self.MAX_VALUE,
                        fuzz=0, flat=0, resolution=0
                    )
                ),
                (
                    ecodes.ABS_RX,
                    AbsInfo(
                        value=self.MIN_VALUE, min=self.MIN_VALUE, max=self.MAX_VALUE,
                        fuzz=0, flat=0, resolution=0
                    )
                ),
                (
                    ecodes.ABS_RY,
                    AbsInfo(
                        value=self.MIN_VALUE, min=self.MIN_VALUE, max=self.MAX_VALUE,
                        fuzz=0, flat=0, resolution=0
                    )
                ),
            ]
        }

        self._allowed_events = [inf[0] for inf in self._events[ecodes.EV_ABS]]

        super().__init__(events=self._events, name=self.NAME, phys=self.NAME)

    def emit(self, values: dict):
        if values:
            for key, value in values.items():
                if key not in self._allowed_events:
                    print("Not allowed event '{}'".format(key))
                    continue
                if value < self.MIN_VALUE:
                    value = self.MIN_VALUE
                elif value > self.MAX_VALUE:
                    value = self.MAX_VALUE

                self.write(ecodes.EV_ABS, key, value)
            self.syn()
