from evdev import InputDevice, list_devices

from utils.singleton import Singleton
from libs.device.emit_device import EmitDevice
from libs.device.listen_device import ListenDevice
from libs.device.exceptions import NoDeviceException


class DeviceManager(metaclass=Singleton):
    # TODO: remove hardcode
    JOYSTICK = "Logitech Logitech Extreme 3D"

    def __init__(self):
        devices = [InputDevice(path) for path in list_devices()]
        self._devices = {device.name: device.path for device in devices}
        self._listen_devices = {}
        self._emit_device = None

    @property
    def devices(self):
        """
        {0: [0, 1, 3, 4],
 1: [288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299],
 3: [(0, AbsInfo(value=510, min=0, max=1023, fuzz=3, flat=63, resolution=0)),
  (1, AbsInfo(value=510, min=0, max=1023, fuzz=3, flat=63, resolution=0)),
  (5, AbsInfo(value=128, min=0, max=255, fuzz=0, flat=15, resolution=0)),
  (6, AbsInfo(value=255, min=0, max=255, fuzz=0, flat=15, resolution=0)),
  (16, AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0)),
  (17, AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))],
 4: [4]}


 type 1 (EV_KEY), code 293  (BTN_PINKIE), value 0
        :return:
        """
        return list(self._devices.keys())

    def get_listen_device(self, name) -> ListenDevice:
        if name not in self._listen_devices:
            if name not in self._devices:
                raise NoDeviceException("Device with name '{}' does not exist".format(name))
            self._listen_devices[name] = ListenDevice(device_path=self._devices[name])
            self._listen_devices[name].start()
        return self._listen_devices[name]

    def get_emit_device(self) -> EmitDevice:
        if not self._emit_device:
            self._emit_device = EmitDevice()
        return self._emit_device

    def stop_devices(self) -> None:
        devices = self._listen_devices
        self._listen_devices = {}
        for device in devices.values():
            device.stop()
            device.join()
