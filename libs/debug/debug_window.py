import dlib

from utils.singleton import Singleton


class DLibDebugWindow(metaclass=Singleton):

    def __init__(self):
        self._window = None

    @property
    def window(self) -> dlib.image_window:
        if not self._window:
            self._window = dlib.image_window()
            self._window.set_title(self.__class__.__name__)
        return self._window
