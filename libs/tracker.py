import cv2
from time import time, sleep
from threading import Thread

from libs.configuration import Configuration, ConfigurationParameter
from libs.device.device_manager import DeviceManager
from libs.face import NoDotsException
from libs.face_recognition import FaceRecognition


class BaseTrackerException(Exception):
    pass


class CannotOpenCameraException(BaseTrackerException):
    pass


class Tracker(Thread):
    EMIT_TIME = 0.01

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._configuration = Configuration()
        self._device_manager = DeviceManager()
        self._face_recognition = None
        self._process = True
        self._camera = None
        self._emit_device = None
        self._face = None

    def start(self):
        if not self._face_recognition:
            self._camera = cv2.VideoCapture(self._configuration[ConfigurationParameter.CAMERA])
            if not self._camera.isOpened():
                raise CannotOpenCameraException()

            self._emit_device = self._device_manager.get_emit_device()

            # TODO: remove hardcode
            self._camera.set(cv2.CAP_PROP_FRAME_WIDTH, 960)
            self._camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
            self._face_recognition = FaceRecognition(camera=self._camera)
            if self._configuration.get(ConfigurationParameter.DISPLAY_FACES):
                self._face_recognition.set_display_image_callback(callback=self.display)
            self._face_recognition.start()
            self._face = self._face_recognition.face

            # waiting for dots in Face object
            while True:
                try:
                    self._face.dots
                    break
                except NoDotsException:
                    sleep(0.1)

        return super().start()

    def stop(self):
        self._process = False

    # TODO: replace temporary display function with Tkinter interface
    def display(self, image):
        cv2.imshow("Display", image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            self.stop()

    def run(self):
        while self._process:
            last_sample_time = time()
            sample = self._face.values
            if sample:
                self._emit_device.emit(values=sample)
            sleep_time = last_sample_time + self.EMIT_TIME - time()
            if self._configuration.get(ConfigurationParameter.DEBUG):
                print("Sleep before getting next face values", sleep_time)
            if sleep_time > 0:
                sleep(sleep_time)

        self._face_recognition.stop()
        self._face_recognition.join()
        self._camera.release()
        cv2.destroyAllWindows()
        self._device_manager.stop_devices()
