class BaseFaceException(Exception):
    pass


class NoDotsException(BaseFaceException):
    pass
