from utils.singleton import Singleton


class FaceConfig(metaclass=Singleton):
    # TODO: replace hardcode, it should be configurable
    """"
    Contains user face parameters: comfortable values of shifting and declination
    """

    def __init__(self):
        self._face = {
            "shift_x": 160,
            "shift_y": 160,
            "distance": 60,
            "rotate_y": 100,
            "rotate_x": 100,
        }

    def __getitem__(self, item):
        return self._face[item]
