import cv2
import numpy as np

from collections import namedtuple
from evdev import ecodes

from libs.face import FaceParameter, NoDotsException
from libs.device.emit_device import EmitDevice

FaceRectangle = namedtuple("FaceRectangle", ("x", "y", "width", "height",))


class Face:
    X = 0
    Y = 1

    rotate_vertical = FaceParameter("rotate_x", EmitDevice.CENTRAL_VALUE)
    rotate_horizontal = FaceParameter("rotate_y", EmitDevice.CENTRAL_VALUE)
    # rotate_vertical = FaceParameter("shift_y", EmitDevice.CENTRAL_VALUE)
    # rotate_horizontal = FaceParameter("shift_x", EmitDevice.CENTRAL_VALUE)
    shift_distance = FaceParameter("distance", EmitDevice.MIN_VALUE)

    AXIS = {
        ecodes.ABS_RX: "rotate_horizontal",
        ecodes.ABS_RY: "rotate_vertical",
        ecodes.ABS_Z: "shift_distance",
    }

    def __init__(self):
        self._dots = None
        self._face_rect = None

    @property
    def dots(self):
        if self._dots is None:
            raise NoDotsException()
        return self._dots

    @dots.setter
    def dots(self, value):
        self._dots = value
        self._face_rect = FaceRectangle(*cv2.boundingRect(np.array(value)))

    @property
    def face_rectangle(self):
        if self._face_rect is None:
            raise NoDotsException()
        return self._face_rect

    @property
    def shift_x(self):
        """"
        Horizontal shift
        """
        return self.face_rectangle.x + self.face_rectangle.width / 2

    @property
    def shift_y(self):
        """"
        Vertical shift
        """
        return self.face_rectangle.y + self.face_rectangle.height / 2

    @property
    def rotate_x(self):
        """"
        Vertical rotation, rotation of X axis
        """
        return self.dots[31][self.Y] - self.face_rectangle.y

    @property
    def rotate_y(self):
        """"
        Horizontal rotation, rotation of Y axis
        """
        return self.dots[31][self.X] - self.face_rectangle.x

    @property
    def distance(self):
        """"
        Vertical shift
        """
        return self.face_rectangle.height

    @property
    def values(self):
        sample = {}

        for axis, parameter in self.AXIS.items():
            value = getattr(self, parameter)
            if value is not None:
                sample[axis] = value

        return sample
