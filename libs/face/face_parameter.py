import numpy as np

from collections import deque

from libs.configuration import Configuration, ConfigurationParameter
from libs.device.emit_device import EmitDevice
from libs.device.device_manager import DeviceManager
from libs.face.face_config import FaceConfig


class FaceParameter:
    DAMPER_BUFFER_LENGTH = 60

    LOW_CHANGE = 1
    HIGH_CHANGE = 3

    def __init__(self, source_value_name, start_position):
        self._config = FaceConfig()
        self._source_value_name = source_value_name
        self._start_position = start_position
        self._start_value = None
        self._last_result = None
        self._update_start_position = True
        global_config = Configuration()

        listen_device = DeviceManager() \
            .get_listen_device(name=global_config[ConfigurationParameter.JOYSTICK_DEVICE_NAME])
        listen_device.subscribe(global_config[ConfigurationParameter.RESET_POSITION_KEY], self.remember_start_position)

        self._values = deque(maxlen=self.DAMPER_BUFFER_LENGTH)

    def remember_start_position(self):
        self._update_start_position = True

    def _calculate_current_value(self, instance):
        current_value = getattr(instance, self._source_value_name)

        if self._update_start_position:
            self._start_value = current_value
            result = self._start_position
            self._update_start_position = False
        else:
            delta = self._config[self._source_value_name]
            min_position = self._start_value - delta * self._start_position / EmitDevice.MAX_VALUE
            absolute_shift = current_value - min_position
            relative_shift = absolute_shift / delta

            if relative_shift < 0:
                relative_shift = 0
            elif relative_shift > 1:
                relative_shift = 1

            result = int((EmitDevice.MAX_VALUE - EmitDevice.MIN_VALUE) * relative_shift)

        return result

    def _calculate_aggregated_value(self):
        result = None
        avg = np.mean(np.array(self._values))
        sigma = np.std(np.array(self._values))
        filtered = list(filter(lambda x: abs(x - avg) <= sigma, self._values))
        if filtered:
            result = int(np.mean(filtered))

        if self._last_result is not None and result is not None:
            delta = result - self._last_result
            if delta == 0:
                return None
            if abs(delta) <= sigma:
                change = self.LOW_CHANGE
            else:
                change = self.HIGH_CHANGE

            if abs(delta) < change:
                change = abs(delta)

            if delta > 0:
                result = self._last_result + change
            else:
                result = self._last_result - change

        if result is not None:
            self._last_result = result

        return result

    def __get__(self, instance, owner):
        self._values.append(self._calculate_current_value(instance=instance))
        return self._calculate_aggregated_value()
