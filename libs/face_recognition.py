import cv2
import dlib
import numpy as np
from imutils import face_utils
from threading import Thread
from time import time, sleep

from libs.face import Face
from libs.configuration import Configuration, ConfigurationParameter


class FaceRecognition(Thread):
    MAX_FACE_RELATIVE_DISTANCE = 0.1
    WAIT_FOR_FACE_DISTANCE_FRAMES = 5

    def __init__(self, camera):
        super().__init__()
        self._display_image_callback = None
        self._display_image = None
        self._configuration = Configuration()
        self._detector = dlib.get_frontal_face_detector()
        self._predictor = dlib.shape_predictor(self._configuration[ConfigurationParameter.FACE_SHAPE_PREDICTOR])
        self._camera = camera

        height, width, _channels = self._get_frame().shape
        self._max_face_distance = np.linalg.norm(np.array([0, 0]) - np.array([width, height])) * \
                                  self.MAX_FACE_RELATIVE_DISTANCE
        self._face_center = np.array([width // 2, height // 2])
        self._wait_for_face_distance_frames = 0
        self._face = Face()

        self._win = None
        self._process = True

    def stop(self):
        self._process = False

    def run(self):
        while self._process:
            last_sample = time()
            self._update_face_shape()
            sleep_time = last_sample + 1 / self._configuration[ConfigurationParameter.MAX_CAMERA_FPS] - time()
            if self._configuration.get(ConfigurationParameter.DEBUG):
                print("Sleep before getting next frame", sleep_time)
            if sleep_time < 0:
                sleep_time = 0.05
            if sleep_time > 0 and self._process:
                sleep(sleep_time)

    def set_display_image_callback(self, callback):
        self._display_image_callback = callback

    def _get_frame(self):
        _, frame = self._camera.read()
        self._display_image = None
        if self._display_image_callback:
            self._display_image = frame.copy()
        return frame

    def _get_face(self, image):
        faces_rects = self._detector(image)

        if self._configuration.get(ConfigurationParameter.DEBUG):
            print("Number of faces detected: {}.".format(len(faces_rects)))
            print("Faces rects", faces_rects)

        user_face = None
        user_face_center = None
        closest_distance = None

        # calculate face center and filter them by distance, distance change should be lower than
        faces = []
        for face in faces_rects:
            center = face.center()
            center = np.array([center.x, center.y])
            distance = np.linalg.norm(self._face_center - center)

            if self._configuration.get(ConfigurationParameter.DEBUG):
                print("Face {face}, distance to the previous is {dst}".format(face=face, dst=distance))

            if self._wait_for_face_distance_frames > 0 and distance > self._max_face_distance:
                # import pdb
                # pdb.set_trace()
                if self._configuration.get(ConfigurationParameter.DEBUG):
                    print("Face {face} is quite far from expected position, distance is {dst}.".format(
                        face=str(face),
                        dst=distance
                    ))
                    if self._display_image is not None:
                        cv2.rectangle(
                            self._display_image,
                            (face.left(), face.top(),), (face.right(), face.bottom(),),
                            (255, 0, 0,),  # BGR
                            1
                        )
                continue

            faces.append(face)
            if user_face is None or closest_distance > distance:
                user_face = face
                closest_distance = distance
                user_face_center = center

        if not faces and faces_rects:
            self._wait_for_face_distance_frames -= 1
            if self._configuration.get(ConfigurationParameter.DEBUG):
                print("Faces were removed by distance, waiting for face {} more frames"
                      .format(self._wait_for_face_distance_frames))

        if user_face:
            self._face_center = user_face_center
            self._wait_for_face_distance_frames = self.WAIT_FOR_FACE_DISTANCE_FRAMES

        if self._display_image is not None:
            secondary_faces = [face for face in faces if face is not user_face]
            if secondary_faces:
                for face in faces:
                    cv2.rectangle(
                        self._display_image,
                        (face.left(), face.top(),), (face.right(), face.bottom(),),
                        (0, 0, 255,),  # BGR
                        1
                    )
            if user_face:
                cv2.rectangle(
                    self._display_image,
                    (user_face.left(), user_face.top(),), (user_face.right(), user_face.bottom(),),
                    (0, 255, 0,),  # BGR
                    1
                )

        if self._configuration.get(ConfigurationParameter.DEBUG):
            print("Found faces", faces)
            print("Closest face is {face}, distance is {dst}".format(face=user_face, dst=closest_distance))

        return user_face

    def _get_face_shape(self, image, rectangle: dlib.rectangle) -> Face:
        # determine the facial landmarks for the face region, then
        shape = self._predictor(image, rectangle)
        # convert the landmark (x, y)-coordinates to a NumPy array
        shape = face_utils.shape_to_np(shape)

        if self._display_image is not None:
            print("Found {} face shape dots".format(len(shape)))
            for dot in shape:
                cv2.circle(
                    self._display_image,
                    (dot[0], dot[1],),
                    1,
                    (255, 255, 255,),
                    1
                )

        self._face.dots = shape

    def _update_face_shape(self):
        image = self._get_frame()
        face_rectangle = self._get_face(image)
        if face_rectangle is not None:
            self._get_face_shape(image=image, rectangle=face_rectangle)
        if self._display_image is not None:
            self._display_image_callback(self._display_image)

    @property
    def face(self) -> Face:
        return self._face

    def _test(self, img, x0, y0, x1, y1):
        if not self._win:
            self._win = dlib.image_window()
        self._win.clear_overlay()
        self._win.set_image(img)
        self._win.add_overlay(dlib.rectangle(x0, y0, x1, y1))
