#!/usr/bin/env python3

from libs.tracker import Tracker
from libs.configuration import Configuration, ConfigurationParameter

if __name__ == "__main__":
    configuration = Configuration()
    configuration.set(ConfigurationParameter.DEBUG, True)
    configuration.set(ConfigurationParameter.DISPLAY_FACES, True)

    tracker = Tracker()
    tracker.start()
    try:
        tracker.join()
    except KeyboardInterrupt:
        tracker.stop()
        tracker.join()
